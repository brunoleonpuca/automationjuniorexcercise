﻿namespace AutomationJunior
{
    using OpenQA.Selenium;

    public class AutomationJuniorPage
    {
        public IWebDriver Driver;
        public AutomationJuniorActions Actions;

        public AutomationJuniorPage(IWebDriver driver)
        {
            Driver = driver;
            Actions = new AutomationJuniorActions(Driver);

        }
    }
}
