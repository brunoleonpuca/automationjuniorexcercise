﻿namespace AutomationJunior
{
    using NUnit.Framework;
    using System;

    public class AutomationJuniorTests : AutomationJuniorTestBase
    {
        [TestCase("Josmer Delgado","QA Automation")]
        public void Excercise(string name, string occupation)
        {
            string title = Page.Actions.GetPageTitle();
            Page.Actions.PastePageTitle(title);
            Page.Actions.PasteName(name);
            Page.Actions.ChangeOccupation(occupation);
            string number = Page.Actions.BlackBoxesCount().ToString();
            Page.Actions.BlackBoxesQuantity(number);
            Page.Actions.ClickMeButton();
            string red = Page.Actions.RedBoxClass();
            Page.Actions.PasteRedBoxClass(red);
            Page.Actions.ClickSubmitButton();
            Page.Actions.CheckResults();

        }
    }
}
