﻿namespace AutomationJunior
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class AutomationJuniorActions
    {
        public AutomationJuniorElements Element;
        public IWebDriver Driver;

        public AutomationJuniorActions(IWebDriver driver)
        {
            Driver = driver;
            Element = new AutomationJuniorElements(Driver);
        }

        public string GetPageTitle()
        {
            return Driver.Title;
        }

        public void PastePageTitle(string title)
        {
            Element.AnswerOneSlot.SendKeys(title);
        }

        public void PasteName(string name)
        {
            Element.NameSlot.SendKeys(name);
        }

        public void ChangeOccupation(string occupation)
        {
            var selector = new SelectElement(Element.OccupationSlot);
            selector.SelectByText(occupation);
        }

        public int BlackBoxesCount()
        {
            return Element.BlackBoxes.Count;
        }

        public void BlackBoxesQuantity(string number)
        {
            Element.AnswerFourSlot.SendKeys(number);
        }

        public void ClickMeButton()
        {
            Element.ClickMeButton.Click();
        }

        public string RedBoxClass()
        {
            return Element.RedBox.GetAttribute("class");
        }
        
        public void PasteRedBoxClass(string red)
        {
            Element.AnswerSixSlot.SendKeys(red);
        }

        public void ClickSubmitButton()
        {
            Element.SubmitButton.Click();
        }

           
        public void CheckResults()
        {
            Element.CheckResults.Click();
        }
    }
}
