﻿namespace AutomationJunior
{
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    public class AutomationJuniorTestBase
    {
        public IWebDriver Driver;
        public AutomationJuniorPage Page;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            Driver = new ChromeDriver();
            Page = new AutomationJuniorPage(Driver);
            Driver.Navigate().GoToUrl("http://exercises.fdvs.com.ar/junior.html");
            //Driver.Manage().Window.FullScreen();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Driver.Close();
            Driver.Dispose();
        }

    }
}
