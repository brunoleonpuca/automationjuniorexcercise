﻿namespace AutomationJunior
{
    using OpenQA.Selenium;
    using System.Collections.ObjectModel;

    public class AutomationJuniorElements
    {
        public IWebDriver Element;

        public AutomationJuniorElements(IWebDriver element)
        {
            Element = element;
        }

        public IWebElement AnswerOneSlot => Element.FindElement(By.Id("answer1"));
        public IWebElement NameSlot => Element.FindElement(By.Id("name"));
        public IWebElement OccupationSlot => Element.FindElement(By.Id("occupation"));
        public ReadOnlyCollection<IWebElement> BlackBoxes => Element.FindElements(By.XPath("//span[@class='blackbox']"));
        public IWebElement AnswerFourSlot => Element.FindElement(By.Id("answer4"));
        public IWebElement ClickMeButton => Element.FindElement(By.XPath("//a[3]"));
        public IWebElement RedBox => Element.FindElement(By.Id("redbox"));
        public IWebElement AnswerSixSlot => Element.FindElement(By.Id("answer6"));
        public IWebElement SubmitButton => Element.FindElement(By.Id("submitbutton"));


        public IWebElement CheckResults => Element.FindElement(By.Id("checkresults"));


    }
}
